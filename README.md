# kbot
Devops repository from scratch

## Description

functional Telegram bot with root command and settings. It will be able to process messages from users and respond to them

## Reference in Telegram

https://t.me/shkirmantsev_bot

### Examples of commands:
- ```/start```
- ```/stop```
- ```/start hello```